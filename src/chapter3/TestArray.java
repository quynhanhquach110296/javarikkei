package chapter3;

public class TestArray {
	public static void main(String args[]) {

//		int arr[] = { 4, 4, 5 };
//
//		Class c = arr.getClass();
//		String name = c.getName();// gán class xong gán name ko gọi đk trực tiếp get name từ 
//
//		System.out.println("Ten: "+name);
		char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e', 'i', 'n', 'a', 't', 'e', 'd' };
		char[] copyTo = new char[7];

		System.arraycopy(copyFrom, 2, copyTo, 0, 7);//method copy cua System
		System.out.println(new String(copyTo));
	}
}
