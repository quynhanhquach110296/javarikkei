package chapter3;

import java.util.ArrayList;
import java.util.List;

public class TestArrayList {
	public static void main(String[] args) {
//		ArrayList list = new ArrayList();
//		list.add("hawk");
//		list.add(Boolean.TRUE);
//		System.out.println(list);
//		ArrayList<String> safer = new ArrayList<>();
//		safer.add("sparrow");
//		safer.add(Boolean.TRUE);
//		System.out.println(safer);
//		List<String> birds = new ArrayList<>();
//		birds.add("hawk");
//		birds.add("hawk"); 
//		System.out.println(birds.remove("cardinal"));
//		System.out.println(birds.remove("hawk"));
//		System.out.println(birds);
//		System.out.println(birds.remove(0)); // prints hawk
//		System.out.println(birds);
//		ArrayList<String> listA = new ArrayList<String>();
//		ArrayList<String> listB = new ArrayList<String>();
//        listB.add("Java");
//        listA.add("Java1");
//        listA.add("Java2");
//        listA.add("Java3");
//        listA.retainAll(listB);
//        System.out.println("listA:"+listA);
//        System.out.println(listB);
//        List<String> birds = new ArrayList<>();
//         birds.add("hawk"); // [hawk]
//         System.out.println(birds.size()); // 1
//         System.out.println(birds);
//         birds.set(0, "robin"); // [robin]
//         System.out.println(birds.size()); // 1
//         System.out.println(birds);
//         birds.set(1, "robin");
//         System.out.println(birds.size());
//         System.out.println(birds);
		int n, k, dem;
		for (k = 10; k <= 12; k++) {
			dem = 0;
			for (n = 2; n <= k; n++) {
				if (k % n == 2) {
					dem++;
				}
				if (dem > 1)
					System.out.print(k);
			}
		}
	}
}
