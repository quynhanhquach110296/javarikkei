package chapter4;

public class TestContructor {
//	private String color;
//	private int height;
//	private int length;
//	public  TestContructor(int length, int theHeight) {
//	length = this.length; // backwards – no good!
//	height = theHeight; // fine because a different name
//	this.color = "white"; // fine, but redundant
//	}
//	public static void main(String[] args) {
//		TestContructor b = new TestContructor(1, 2);
//	 System.out.println(b.length + " " + b.height + " " + b.color);
//	 }
		private int numTeeth;
		private int numWhiskers;
		private int weight;
		public TestContructor(int weight) {
		this(weight, 16); // calls constructor with 2 parameters
		}
		public TestContructor(int weight, int numTeeth) {
		this(weight, numTeeth, 6); // calls constructor with 3 parameters
		}
		public TestContructor(int weight, int numTeeth, int numWhiskers) {
		this.weight = weight;
		this.numTeeth = numTeeth;
		this.numWhiskers = numWhiskers;
		}
		public void print() {
		System.out.println(weight + " " + numTeeth + " " + numWhiskers);
		}
		public static void main(String[] args) {
			TestContructor testContructor = new TestContructor(15);
			testContructor.print();
		}
}
